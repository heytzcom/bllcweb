/**
 * Created by pcbeta on 15/7/6.
 */
Meteor.methods({
    getLocationInfor: function (lng, lat) {
        /*根据经纬度获取地理位置*/
        console.log(lat, lng);
        //http://api.map.baidu.com/geocoder/v2/?ak=tgwAx093dLhSmatYvujmZ8PT&callback=renderReverse&location=31.24423415402998,121.4382962511121&output=json
        var options = {
            contentType: 'application/json',
            params: {
                ak: 'tgwAx093dLhSmatYvujmZ8PT',
                callback: 'renderReverse',
                //location: '31.24423415402998,121.4382962511121',
                location: lat + ',' + lng,
                output: 'json'
            }
        };
        var response = HTTP.get("http://api.map.baidu.com/geocoder/v2/", options);
        if (response.statusCode == 200) {
            var con = response.content;
            con = con.substring(29, con.length - 1);
            console.log("==============getlocationInfro==============" +
                "" + con);
            return JSON.parse(con);
        }
    },
    //成功返回天气内容对象，失败返回null
    getWeathersByCity: function (city) {
        var response = HTTP.get("https://api.thinkpage.cn/v2/weather/all.json?", {
            params: {
                city: city, //北京｜220.181.111.86｜39.93:116.40 （格式为：纬度:经度）
                language: 'zh-chs',
                unit: 'c',
                aqi: 'city',
                //key: 'CHW8ZIIRNB'
                key: 'ZO7WXHFI72'
            }
        });
        return (response.data && response.data);
    }
});