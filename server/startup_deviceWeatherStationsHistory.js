/**
 * Created by chendongdong on 15/11/17.
 */


function getBeforeDate(n) {
    var n = n;
    var d = new Date();
    var year = d.getFullYear();
    var mon = d.getMonth() + 1;
    var day = d.getDate();
    if (day <= n) {
        if (mon > 1) {
            mon = mon - 1;
        }
        else {
            year = year - 1;
            mon = 12;
        }
    }
    d.setDate(d.getDate() - n);
    year = d.getFullYear();
    mon = d.getMonth() + 1;
    day = d.getDate();
    s = year + "-" + (mon < 10 ? ("0" + mon) : mon) + "-" + (day < 10 ? ('0' + day) : day);
    return s;
}


function WeatherStationsHistory() {
    console.log("-----------------WeatherStationsHistory-----------------");
    User_data.find({}).forEach(function (user) {
        //获取室外的PM25
        //获取用户所在的城市天气信息
        var cityWeather = Weathers.find({city_name: user.city}).fetch();
        var cityWeatherNow = cityWeather[cityWeather.length - 1];
        //在用户的城市天气信息中获得室外的pm25值
        var pm25Out;
        if (cityWeatherNow && cityWeatherNow.now && cityWeatherNow.now.air_quality) {

            pm25Out = (cityWeatherNow.now.air_quality.city.pm25 == null || cityWeatherNow.now.air_quality.city.pm25 == undefined)
                ? 0 : parseInt(cityWeatherNow.now.air_quality.city.pm25);
        }
        var pm25in = 0;
        //当前日期的时间格式化操作
        var dateNow = new Date().Format("yyyy-MM-dd");
        //获取前八天日期
        var lasetDate = getBeforeDate(8);
        console.log(user.uid);
        // 通过遍历devices来获取室内的pm25得值
        Devices.find({uid: user.uid}).forEach(function (device) {
            // 根据产品ID来获取appid
            var productkey = device.productkey;
            if (productkey == Meteor.settings.GizwitsKey.jh.productKey) {
                var appid = Meteor.settings.GizwitsKey.jh.appId;
            } else if (productkey == Meteor.settings.GizwitsKey.xf.productKey) {
                var appid = Meteor.settings.GizwitsKey.xf.appId;
                return;
            }
            //通过调用机制云的方法来获得室内的数据
            var devdata = JSON.parse(Meteor.call("gizwitsGet", "devdata/" + device.did + "/latest", null, appid));
            //如果对象存在并且没有error_code
            if (devdata && !devdata.error_code) {
                console.log('-------getDevdataLatest-------success--------');
                if (devdata.attr) {
                    pm25in = (devdata.attr.PM25 == null) || (devdata.attr.PM25 == undefined) ? 0 : parseInt(devdata.attr.PM25);
                }
                // 查找当天所有的历史数据
                temperature.find({mac: device.mac, date: dateNow}).forEach(function (historyData) {
                    pm25in = (pm25in + historyData.pm25In) / 2;
                });
                //清空当天该设备的所有数据
                temperature.remove({mac: device.mac, date: dateNow});
                // 移除历史第八天的数据
                temperature.remove({date: lasetDate});
                // 插入今天的最新数据
                temperature.insert({
                    mac: device.mac,
                    pm25Out: pm25Out,
                    pm25In: pm25in,
                    date: dateNow
                });
            }
        });
    });
}
Meteor.setInterval(WeatherStationsHistory, 1000 * 60 * 60 * 2);