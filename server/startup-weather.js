/**
 * Created by chendongdong on 15/11/17.
 */

function getWeatherByHours() {
    var dd = new Date().toLocaleTimeString();
    if (Number(dd.slice(0, 2)) > 12) {
        Weathers.remove();
    }
    Meteor.settings.cities.forEach(function (city) {
        if (city.name == city.parent1) {
            try {
                var weatherObj = Meteor.call("getWeathersByCity", city.name);
                Weathers.insert(weatherObj.weather[0]);
            } catch (e) {
                console.log("获取天气失败!", city.name);
            }
        }
    });
    //todo: 传入city参数，代替上海，代码参见上面注释部分
    //var weatherObj = Meteor.call("getWeathersByCity", '上海');
    //Weathers.insert(weatherObj.weather[0]);
    Meteor.setTimeout(getWeatherByHours, 1000 * 60 * 60 * 12);
}

