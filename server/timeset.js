/**
 * Created by zhang on 15-3-30.
 */



Meteor.methods({

    //开启高污染自启动后传入str，关闭str则为null
    pmAutoRun: function (_id, did, str) {
        if (_id && did) {
            var dev = Controls.findOne({uid: _id, did: did});
            if (dev) {
                Controls.update({uid: _id, did: did}, {$set: {pm25: str}});
            } else {
                Controls.insert({uid: _id, did: did, pm25: str});
            }
        } else {
            console.log('----set----pm25----failed----')
        }
    },
    //time参数为UTC时间，在北京时间的基础上减去8小时，再传入
    addTimeSet: function (did, date, time, mode, _id) {
        console.log('----addTimeSet----start----');
        var user = Meteor.call('getUserDataById', _id);
        if (user == null) return false;
        var product_key = Devices.findOne({uid: _id, did: did}).productkey;
        if (product_key == Meteor.settings.GizwitsKey.jh.productKey) {
            var appid = Meteor.settings.GizwitsKey.jh.appId;
            var token = user.gizwitsUserData.token;
        } else if (product_key == Meteor.settings.GizwitsKey.xf.productKey) {
            var appid = Meteor.settings.GizwitsKey.xf.appId;
            var token = user.gizwitsUserDataXF.token;
        }

        var result = Meteor.call("gizwitsTokenPost", "scheduler", {
            "date": date, "time": time, "repeat": 'none',
            "task": [{"did": did, "product_key": product_key, "attrs": {"Mode": mode}}],
            "retry_count": 4, "retry_task": "all"
        }, token, appid);

        console.log(result);
    },

    getTimeSetList: function (_id, limit, skip, did) {
        console.log("getTimeSetList----start");
        var user = Meteor.call('getUserDataById', _id);
        if (!skip) skip = 0;
        if (!limit) limit = 20;
        var productkey = Devices.findOne({uid: _id, did: did}).productkey;
        if (productkey == Meteor.settings.GizwitsKey.jh.productKey) {
            var appid = Meteor.settings.GizwitsKey.jh.appId;
            var token = user.gizwitsUserData.token;
        } else if (productkey == Meteor.settings.GizwitsKey.xf.productKey) {
            var appid = Meteor.settings.GizwitsKey.xf.appId;
            var token = user.gizwitsUserDataXF.token;
        }
        var list = JSON.parse(Meteor.call("gizwitsTokenGet",
            "scheduler", {"limit": limit, "skip": skip}, token, appid));
        if (list && !list.error_code) {
            console.log('----getTimeSetList----success----');
            Time_set.remove({uid: _id, did: did});
            for (var i = 0; i < list.length; i++) {
                if (list[i].task[0].did == did) {
                    list[i].uid = _id;
                    list[i].did = did;
                    Time_set.insert(list[i])
                }
            }
        }
    },

    deleteTimeSet: function (_id, tid, did) {
        var user = Meteor.call('getUserDataById', _id);
        var productkey = Devices.findOne({uid: _id, did: did}).productkey;
        if (productkey == Meteor.settings.GizwitsKey.jh.productKey) {
            var appid = Meteor.settings.GizwitsKey.jh.appId;
            var token = user.gizwitsUserData.token;
        } else if (productkey == Meteor.settings.GizwitsKey.xf.productKey) {
            var appid = Meteor.settings.GizwitsKey.xf.appId;
            var token = user.gizwitsUserDataXF.token;
        }
        Meteor.call('gizwitsDelete', 'scheduler/' + tid, null, token, appid);
    }

    //getTimeSetLog: function (_id) {
    //    var user = Meteor.call('getUserDataById', _id);
    //    if (user == null) return false;
    //    var tid = '553da2e3f707a3109462bea0';
    //    var token = user.gizwitsUserData.token;
    //    var data = null;
    //    var timeLog = JSON.parse(Meteor.call("gizwitsTokenGet",
    //        "scheduler/"+tid+"/logs",data,token));
    //    if (timeLog.error_code == null || timeLog.error_code == undefined) {
    //        console.log(timeLog);
    //        for (var i in timeLog) {
    //            console.log('//----objintimelog----'+timeLog[i]+"----"+i);
    //        }
    //    }
    //}

});