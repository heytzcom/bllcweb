/**
 * Created by vproUsr on 15-7-1.
 */
Array.prototype.deleteElementByValue = function (varElement) {
    var numDeleteIndex = -1;
    for (var i = 0; i < this.length; i++) {
        // 严格比较，即类型与数值必须同时相等。
        if (this[i] === varElement) {
            this.splice(i, 1);
            numDeleteIndex = i;
            break;
        }
    }
    return numDeleteIndex;
}

//HTTP.methods({
//    '/hello/:openid': {
//        get: function () {
//            console.log("........", this.params.openid);
//            return "....";
//        }
//        // get: function
//    }
//});

Meteor.methods({
    bindingWeiXin: function (account, password, openid) {
        console.log('in binding');
        console.log('account:' + account);
        console.log('password:' + password);
        console.log('openid:' + openid);
        var user_data = User_data.findOne({username: account, pwd: password});//是否有账号
        var open_id = Openid.findOne({openid: openid});                      //微信账号是否已经有绑定
        var username = Openid.findOne({username: account});                  //用户输入的账号是否已经被微信绑定
        var isBinding = Openid.findOne({username: account, openid: openid});   //用户输入的账号是否已经和此微信绑定过
        console.log('user_data:' + user_data);
        console.log('username:' + username);
        console.log(user_data && open_id && username);
        if (!user_data) {
            console.log('notFound');
            return 'notFound';
        }
        if (user_data && !open_id && !username) {
            console.log('in insert');
            Openid.insert({openid: [openid], username: account, password: password});
            return 'insert';
        } else if (user_data && !open_id && username) {
            var open_idArr = username.openid;
            open_idArr.push(openid);
            Openid.update({"username": account}, {"$set": {openid: open_idArr}})
            console.log('update');
            return 'insert';
            //}
            //console.log('not openid');
        } else if (isBinding) {
            console.log('isBinding');
            return 'isBinding';
        } else if (open_id && !isBinding) {
            console.log('重新绑定');
            var openidArr = open_id.openid;
            if (openidArr) {
                if (openidArr.length == 1) {
                    console.log('解除没有openid的账号');
                    Openid.remove({openid: openid});
                } else {
                    openidArr.deleteElementByValue(openid);
                    console.log('openidArr:' + openidArr);
                    Openid.update({"username": open_id.username}, {"$set": {openid: openidArr}});
                }
            }
            if (!open_id || !username) {//当微信账号没有绑定
                Openid.insert({openid: [openid], username: account, password: password});
                console.log('重新绑定新的账号');
                return 'insert';
            } else if (username) {//当微信账号有绑定
                var open_idArr = username.openid;
                open_idArr.push(openid);
                Openid.update({"username": account}, {"$set": {openid: open_idArr}});
                console.log('重新绑定已经被绑定过的账号');
                return 'insert';
            }
        }
    },

    unbindWeiXin: function (openid) {
        var open_id = Openid.findOne({openid: openid});
        if (open_id) {
            var openidArr = open_id.openid;
            if (openidArr) {
                if (openidArr.length == 1) {
                    console.log('解除没有openid的账号');
                    Openid.remove({openid: openid});
                    return 'ok';
                } else {
                    openidArr.deleteElementByValue(openid);
                    console.log('openidArr:' + openidArr);
                    Openid.update({"username": open_id.username}, {"$set": {openid: openidArr}});
                    return 'ok';
                }
            }
        }
    }
})