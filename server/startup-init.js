/**
 * Created by zhang on 15-6-12.
 */


Meteor.startup(function () {

    //if (!Meteor.users.findOne({username: '18717926783'})) {
    //    Accounts.createUser({
    //        username: '18717926783',
    //        password: '111111',
    //        passwordText: '111111'
    //    });
    //}if (!Meteor.users.findOne({username: '13633929513'})) {
    //    Accounts.createUser({
    //        username: '13633929513',
    //        password: '123456',
    //        passwordText: '123456'
    //    });
    //}

    //加载城市列表
    if (CityList.find().count() == 0) {
        var cityListAll = Meteor.settings.cities;
        for (var i = 0; i < cityListAll.length; i++) {
            if (cityListAll[i].name == cityListAll[i].parent1) {
                CityList.insert(cityListAll[i]);
            }
        }
        console.log('-----CityList----inserted------')
    }
    //加载各种模式
    if (ModeInformation.find().count() == 0) {
        var data = [
            {
                "product": "jh",
                "mode": 0,
                "onoff": "../image/011.png",
                "sleep": "../image/022.png",
                "inter": "../image/033.png",
                "wind": "../image/044.png"
            },
            {
                "product": "jh",
                "mode": 1,
                "onoff": "../image/011on.png",
                "sleep": "../image/022on.png",
                "inter": "../image/033.png",
                "wind": "../image/044.png"
            },
            {
                "product": "jh",
                "mode": 2,
                "onoff": "../image/011on.png",
                "sleep": "../image/022.png",
                "inter": "../image/033on.png",
                "wind": "../image/044.png"
            },
            {
                "product": "jh",
                "mode": 3,
                "onoff": "../image/011on.png",
                "sleep": "../image/022.png",
                "inter": "../image/033.png",
                "wind": "../image/044on1.png"
            },
            {
                "product": "jh",
                "mode": 4,
                "onoff": "../image/011on.png",
                "sleep": "../image/022.png",
                "inter": "../image/033.png",
                "wind": "../image/044on2.png"
            },
            {
                "product": "jh",
                "mode": 5,
                "onoff": "../image/011on.png",
                "sleep": "../image/022.png",
                "inter": "../image/033.png",
                "wind": "../image/044on3.png"
            },
            //1.高速，2.低速，3.间歇，4.加湿，5.关闭
            {
                "product": "xf",
                "mode": 5,
                "onoff": "../image/xf/4_03.png",
                "inter": "../image/xf/3_03.png",
                "low": "../image/xf/2_03.png",
                "high": "../image/xf/1_03.png"
            },
            {
                "product": "xf",
                "mode": 1,
                "onoff": "../image/xf/4_03.png",
                "inter": "../image/xf/3_03.png",
                "low": "../image/xf/2_03.png",
                "high": "../image/xf/5_03.png"
            },
            {
                "product": "xf",
                "mode": 2,
                "onoff": "../image/xf/4_03.png",
                "inter": "../image/xf/3_03.png",
                "low": "../image/xf/6_03.png",
                "high": "../image/xf/1_03.png"
            },
            {
                "product": "xf",
                "mode": 3,
                "onoff": "../image/xf/4_03.png",
                "inter": "../image/xf/7_03.png",
                "low": "../image/xf/2_03.png",
                "high": "../image/xf/1_03.png"
            },
            {
                "product": "xf",
                "mode": 4,
                "onoff": "../image/xf/8_03.png",
                "inter": "../image/xf/3_03.png",
                "low": "../image/xf/2_03.png",
                "high": "../image/xf/1_03.png"
            }
        ];
        _.each(data, function (d) {
            ModeInformation.insert(d);
        });
        console.log('-----ModeInformation----inserted------')
    }
    //加载模拟推送列表
    //if (PushMSGList.find().count() == 0) {
    //
    //    var pushData = [
    //        {
    //            "uid": "JrGQyf9vkmZS5F6g9",
    //            "date": "2015-4-3",
    //            "content": " 据上海市环境保护局官方微博最新消息，上海实时空气质量指数为238，重度污染",
    //            "state": "1"
    //        },
    //        {
    //            "uid": "JrGQyf9vkmZS5F6g9",
    //            "date": "2015-4-8",
    //            "content": " 据上海市环境保护局官方微博最新消息，上海实时空气质量指数为238，重度污染",
    //            "state": "0"
    //        },
    //        {
    //            "uid": "JrGQyf9vkmZS5F6g9",
    //            "date": "2015-4-9",
    //            "content": " 据上海市环境保护局官方微博最新消息，上海实时空气质量指数为238，重度污染",
    //            "state": "1"
    //        },
    //        {
    //            "uid": "JrGQyf9vkmZS5F6g9",
    //            "date": "2015-4-9",
    //            "content": " 据上海市环境保护局官方微博最新消息，上海实时空气质量指数为238，重度污染",
    //            "state": "0"
    //        },
    //        {
    //            "uid": "JrGQyf9vkmZS5F6g9",
    //            "date": "2015-4-9",
    //            "content": " 据上海市环境保护局官方微博最新消息，上海实时空气质量指数为238，重度污染",
    //            "state": "1"
    //        }
    //    ];
    //    _.each(pushData, function (pd) {
    //        PushMSGList.insert(pd);
    //    });
    //    console.log('-----PushMSGList----inserted------')
    //}


});