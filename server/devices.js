/**
 * Created by zhang on 15-3-27.
 */

Meteor.methods({
    addDeviceName: function(_id, did, name) {
        console.log("----addDeviceName----success----"+name);
        Device_name.update({uid: _id, did: did}, {$set: {deviceName: name}});
    },

    //硬件必须在机制云注册以后才可以调用这个接口,remark默认为空！
    bindDevice: function(productkey, mac, remark, _id) {
        console.log("----------bindDevice-------------\n",productkey,mac,remark,_id);
        var appid,token;
        try {
            var user = Meteor.call('getUserDataById', _id);
            if (productkey == Meteor.settings.GizwitsKey.jh.productKey) {
                  appid = Meteor.settings.GizwitsKey.jh.appId;
                  token=user.gizwitsUserData.token;
            } else if (productkey == Meteor.settings.GizwitsKey.xf.productKey) {
                  appid = Meteor.settings.GizwitsKey.xf.appId;
                  token=user.gizwitsUserDataXF.token;
            }
            console.log('----token----',token);
            var device=JSON.parse(Meteor.call(
                "gizwitsGet","devices",{"product_key":productkey,"mac":mac},appid));
            if(device && !device.error_code) {
                console.log("token========"+token);
                console.log("appid--------"+appid);
                var bindingState=JSON.parse(Meteor.call('gizwitsTokenPost','bindings',{"devices":
                    [{"did":device.did,"passcode":device.passcode,"remark":remark}]},token,appid));
                if (bindingState.failed.length == 0) {
                    console.log('-------bind-------success--------');
                } else {
                    console.log('-------bind-------failed--------');
                }
            }
        }catch (e){
            console.log("bindDevice--------"+ e.message);
        }
    },

    unbindDevice: function(_id, did) {
        var user = Meteor.call('getUserDataById', _id);
        var appid,token;
        if(user==null) return false;
        var productkey = Devices.findOne({uid: _id, did: did}).productkey;
        if (productkey == Meteor.settings.GizwitsKey.jh.productKey) {
              appid = Meteor.settings.GizwitsKey.jh.appId;
              token=user.gizwitsUserData.token;
        } else if (productkey == Meteor.settings.GizwitsKey.xf.productKey){
              appid = Meteor.settings.GizwitsKey.xf.appId;
              token=user.gizwitsUserDataXF.token;
        }
        try {
            var device=JSON.parse(Meteor.call('gizwitsDelete',
                'bindings',{"devices":[{"did":did}]},token,appid));
            if (device.failed.length == 0) {
                console.log('-------unbind-------success--------');
            } else {
                console.log('-------unbind-------error--------')
            }
        } catch (e){
            console.log(e.message);
        }
    },

    // todo 对设备绑定后的列表更新会有1分钟左右的延迟
    getDeviceList: function(limit, skip, _id) {
        var user = Meteor.call('getUserDataById', _id);
        if(user==null) return false;
        if(limit==null) limit=20;
        if(skip==null) skip=0;
        try {
            var devices = JSON.parse(Meteor.call(
                "gizwitsTokenGet","bindings",{"limit":limit,"skip":skip},
                user.gizwitsUserData.token, Meteor.settings.GizwitsKey.jh.appId));
            var devicesXF = JSON.parse(Meteor.call(
                "gizwitsTokenGet","bindings",{"limit":limit,"skip":skip},
                user.gizwitsUserDataXF.token, Meteor.settings.GizwitsKey.xf.appId));
            console.log("================================");
            if(devices && !devices.error_code) {
                console.log('----get----devices----success----');
                Devices.remove({uid: _id, productkey: Meteor.settings.GizwitsKey.jh.productKey});
                var devicesArr = devices.devices;
                for (var i=0; i<devicesArr.length; i++) {
                    Devices.insert({
                        uid: _id, productkey: devicesArr[i].product_key,
                        mac: devicesArr[i].mac, did: devicesArr[i].did,
                        passcode: devicesArr[i].passcode,
                        is_online: devicesArr[i].is_online,
                        updated_at: null, attr: null, data: null
                    });
                    var dev = Device_name.findOne({uid: _id, did: devicesArr[i].did});
                    if (!dev) {
                        Device_name.insert({uid: _id, did: devicesArr[i].did, deviceName: '我的设备'});
                    }
                }
            }

            if(devicesXF && !devicesXF.error_code) {
                console.log('----get----devicesXF----success----');
                Devices.remove({uid: _id, productkey: Meteor.settings.GizwitsKey.xf.productKey});
                var devicesArr = devicesXF.devices;
                for (var i=0; i<devicesArr.length; i++) {
                    Devices.insert({
                        uid: _id, productkey: devicesArr[i].product_key,
                        mac: devicesArr[i].mac, did: devicesArr[i].did,
                        is_online: devicesArr[i].is_online,
                        passcode: devicesArr[i].passcode,
                        updated_at: null, attr: null, data: null
                    });
                    var dev = Device_name.findOne({uid: _id, did: devicesArr[i].did});
                    if (!dev) {
                        Device_name.insert({uid: _id, did: devicesArr[i].did, deviceName: '我的设备'});
                    }
                }
            }
        }catch (e) {
            console.log("//----getDeviceList----"+e.message);
        }
    },

    getDevdataLatest: function (_id, did) {
        if (!_id) return;
        var appid;
        if(Devices.findOne({uid: _id, did: did})){
            var productkey = Devices.findOne({uid: _id, did: did}).productkey;
            if (productkey == Meteor.settings.GizwitsKey.jh.productKey) {
                  appid = Meteor.settings.GizwitsKey.jh.appId;
            } else if (productkey == Meteor.settings.GizwitsKey.xf.productKey){
                  appid = Meteor.settings.GizwitsKey.xf.appId;
            }

            var devdata = JSON.parse(Meteor.call("gizwitsGet", "devdata/" + did + "/latest", null, appid));

            //console.log("\n","设备的数据",devdata,"\n----------");
            if (devdata && !devdata.error_code) {
                //console.log('-------getDevdataLatest-------success--------');
                Devices.update({uid: _id, did: did}, {
                    $set: {
                        updated_at: devdata.updated_at,
                        attr: devdata.attr
                    }
                });
            }
        }
    },

    //API文档上已经没有这个接口了
    getDevdata2: function(_id,did,start_ts,limit,skip) {
        if(_id && did && start_ts) {
            if(!limit) limit=20;
            if(!skip) skip=0;
            var product_key = Devices.findOne({uid: _id, did: did}).productkey;
            if (product_key == Meteor.settings.GizwitsKey.jh.productKey) {
                var appid = Meteor.settings.GizwitsKey.jh.appId;
            } else if (product_key == Meteor.settings.GizwitsKey.xf.productKey){
                var appid = Meteor.settings.GizwitsKey.xf.appId;
            }
            var params={
                "product_key":product_key, "did": did, "start_ts":start_ts,
                "name":"PM25","limit":limit,"skip":skip
            };
            var devdata2List = JSON.parse(Meteor.call("gizwitsGet","devdata2",params,appid));
            if (devdata2List && !devdata2List.error_code) {
                console.log('------------getDevdata2----------success');
                Devices.update({uid: _id, did: did}, {
                    $set: {
                        data: devdata2List.data
                    }
                });
            }
        }
    },

    //如果控制成功返回空!!Mode必须为："2" 这样的格式！
    deviceModeControl: function (_id, did, mode) {
        if(!_id) return;
        this.unblock();
        var user = Meteor.call('getUserDataById', _id);

        var productkey = Devices.findOne({uid: _id, did: did}).productkey;
        console.log("device productKey-----"+Devices.findOne({uid:_id}).productkey);
        if (productkey == Meteor.settings.GizwitsKey.jh.productKey) {
            var appid = Meteor.settings.GizwitsKey.jh.appId;
            var token = user.gizwitsUserData.token;
        } else if (productkey == Meteor.settings.GizwitsKey.xf.productKey){
            var appid = Meteor.settings.GizwitsKey.xf.appId;
            var token = user.gizwitsUserDataXF.token;
        }
        var data = '{"attr":"Mode","val":"'+mode+'"}';
        var result = Meteor.call("gizwitsTokenPost","control/"+did,data,token,appid);
        if(result) {
            throw new Meteor.Error(result.error_code, result.error_message);
        } else {
            console.log('---------mode--------control----------success---------');
        }
    }

});