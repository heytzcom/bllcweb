/**
 * Created by zhang on 15-3-18.
 */



//只publish用户所请求的城市数据到client端
Meteor.publish('weathers', function (_id) {
    var user = User_data.findOne({uid: _id});
    if (user&&user.city) {
        return Weathers.find({city_name: user.city});
    }

});

Meteor.publish('devices', function (_id, type) {
    switch (type) {
        case "jh":
            return Devices.find({uid: _id, productkey: Meteor.settings.GizwitsKey.jh.productKey});
        case "xf":
            return Devices.find({uid: _id, productkey: Meteor.settings.GizwitsKey.xf.productKey});
        default:
            return Devices.find({uid: _id});
    }
});

Meteor.publish('device_name', function (_id) {
    return Device_name.find({uid: _id});
});

Meteor.publish('time_set', function (_id) {
    return Time_set.find({uid: _id});
});

Meteor.publish('modeInformation', function () {
    return ModeInformation.find();
});

Meteor.publish('city_list', function () {

    return CityList.find();
});

Meteor.publish('pushMSGList', function (uid) {
    return PushMSGList.find({uid: uid});
});

Meteor.publish('user_data', function () {
    return User_data.find();
});

Meteor.publish("controls", function (_id) {
    return Controls.find({uid: _id});
});

Meteor.publish('openid', function (openid) {
    return Openid.find({openid: openid});
});

Meteor.publish("temperature", function (mac) {
    return temperature.find({mac: mac});
});

