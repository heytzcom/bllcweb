/**
 * Created by chendongdong on 15/11/17.
 */

//高污染自启动server端的轮询, 4 minutes
Meteor.setInterval(function () {
    var devArr = Controls.find({pm25: {$ne: null}}).fetch();
    for (var i = 0; i < devArr.length; i++) {
        var _this = devArr[i];
        var dev = Devices.findOne({uid: _this.uid, did: _this.did});
        if (dev && dev.attr && dev.attr.PM25) {
            if (Number(dev.attr.PM25) >= Number(_this.pm25)) {
                Meteor.call("deviceModeControl", _this.uid, _this.did, "1");
                console.log("----pm25----deviceModeControl----1----");
            }
        } else {
            //console.log("----no----pm25----data----");
        }
    }
}, 1000 * 240);