/**
 * Created by heytz.mac on 15-3-22.
 */



Meteor.methods({

    gizwitsPost: function (action, data, appid) {
        this.unblock();
        var options = {
            contentType: 'application/json',
            headers: {'X-Gizwits-Application-Id': appid},
            data: data
        };
        try {
            var response = HTTP.post(Meteor.settings.baseUrlGizwitsApi + "/" + action, options);
            return (response.content && response.content);
        } catch (e) {
            if (e.response.data) {
                return e.response.data && e.response.data;
            }
        }
    },

    gizwitsTokenPost: function (action, data, token, appid) {
        this.unblock();
        var options = {
            contentType: 'application/json',
            headers: {
                'X-Gizwits-Application-Id': appid,
                'X-Gizwits-User-token': token
            },
            data: data
        };
        try {
            var response = HTTP.post(Meteor.settings.baseUrlGizwitsApi + "/" + action, options);
            return (response.content && response.content);
        } catch (e) {
            if (e.response.data) {
                return e.response.data && e.response.data;
            }
        }
    },

    gizwitsGet: function (action, params, appid) {
        this.unblock();
        var options = {
            contentType: 'application/json',
            headers: {'X-Gizwits-Application-Id': appid},
            params: params
        };
        try {
            var response = HTTP.get(Meteor.settings.baseUrlGizwitsApi + "/" + action, options);
            return (response.content && response.content);
        } catch (e) {
            if (e.response.data) {
                return e.response.data && e.response.data;
            }
        }
    },

    gizwitsTokenGet: function (action, params, token, appid) {
        this.unblock();
        var options = {
            contentType: 'application/json',
            headers: {
                'X-Gizwits-Application-Id': appid,
                'X-Gizwits-User-token': token
            },
            params: params
        };
        try {
            var response = HTTP.get(Meteor.settings.baseUrlGizwitsApi + "/" + action, options);
            return (response.content && response.content);
        } catch (e) {
            if (e.response.data) {
                return e.response.data && e.response.data;
            }
        }
    },

    gizwitsPut: function (action, data, token, appid) {
        this.unblock()
        var options = {
            contentType: 'application/json',
            headers: {
                'X-Gizwits-Application-Id': appid,
                'X-Gizwits-User-token': token
            },
            data: data
        }
        try {
            var response = HTTP.put(Meteor.settings.baseUrlGizwitsApi + '/' + action, options);
            return (response.content && response.content);
        } catch (e) {
            if (e.response.data) {
                return e.response.data && e.response.data;
            }
        }

    },

    gizwitsDelete: function (action, data, token, appid) {
        this.unblock();
        var options = {
            contentType: 'application/json',
            headers: {
                'X-Gizwits-Application-Id': appid,
                'X-Gizwits-User-token': token
            },
            data: data
        }
        try {
            var response = HTTP.del(Meteor.settings.baseUrlGizwitsApi + '/' + action, options);
            return (response.content && response.content);
        } catch (e) {
            if (e.response.data) {
                return e.response.data && e.response.data;
            }
        }

    },

});

GizwitsMethods = {
    AnonymousRegistration: function (gizAppId, username) {
        var options = {
            contentType: 'application/json',
            headers: {'X-Gizwits-Application-Id': gizAppId},
            data: {"phone_id": username}
        };
        try {
            var response = HTTP.post("http://api.gizwits.com/app/users", options);
            if (response.statusCode == 200) {

            } else if (response.statusCode == 201) {
                console.log("201(已创建)请求成功并且服务器创建了新的资源。");
                var jsonData = JSON.parse(response.content);
                console.log("匿名刷新token:", jsonData);

                return jsonData;
            } else {
                console.log('匿名刷新token error: ' + response.statusCode);
                console.log(response);
                return response.content;
            }
        } catch (e) {
            console.warn("AnonymousRegistration:", e);
        }
    }
};