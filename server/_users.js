/**
 * Created by infapp on 15-3-19.
 */

if (Meteor.isServer) {

    //Options中加入用户密码：因为MongoDb不允许获取用户密码，
    //这样就不能去机制云注册，所以在User里面加入一个明文的passwordText字段！
    Accounts.onCreateUser(function (options, user) {


        var gARUserData = GizwitsMethods.AnonymousRegistration(Meteor.settings.GizwitsKey.jh.appId, options.username);
        var gARUserDataxf = GizwitsMethods.AnonymousRegistration(Meteor.settings.GizwitsKey.xf.appId, options.username);
        if (gARUserData && gARUserDataxf) {
            User_data.upsert({username: options.username}, {
                $set: {
                    uid: user._id,
                    pwd: options.passwordText,
                    gizwitsUserData: gARUserData,
                    gizwitsUserDataXF: gARUserDataxf,
                    city: null
                }
            });
        }

        return user;

        //var gUserData = Meteor.call('gizwitsPost', 'users',
        //    {"username": options.username, "password": options.passwordText},
        //    Meteor.settings.GizwitsKey.jh.appId);
        //var gUserDataXF = Meteor.call('gizwitsPost', 'users',
        //    {"username": options.username, "password": options.passwordText},
        //    Meteor.settings.GizwitsKey.xf.appId);
        //if (gUserData.error_code || gUserDataXF.error_code) {
        //    console.log("user----created----on----gizwits----err");
        //    //这个判断是代表用户已经在机制云注册。如果用户输入的密码和机制云的不一直将会报错
        //    // 9019只提示用户名错误，不提示密码错误
        //    if (gUserData.error_code == 9019 && gUserDataXF.error_code == 9019) {
        //
        //        User_data.upsert({username: options.username}, {
        //            $set: {
        //                uid: user._id,
        //                pwd: options.passwordText,
        //                gizwitsUserData: null,
        //                gizwitsUserDataXF: null,
        //                city: null
        //            }
        //        });
        //        console.log('err----user----has----been----created');
        //        return user;
        //    } else {
        //        throw new Error(gUserData.error_code, gUserData.error_message)
        //    }
        //} else {
        //    User_data.upsert({username: options.username}, {
        //        $set: {
        //            uid: user._id,
        //            pwd: options.passwordText,
        //            gizwitsUserData: null,
        //            gizwitsUserDataXF: null,
        //            city: null
        //        }
        //    });
        //    console.log("user----created----on----gizwits----success");
        //    return user;
        //}
    });

    Accounts.onLogin(function (obj) {

        var user = Meteor.users.findOne({_id: obj.user._id});
        var gARUserData = GizwitsMethods.AnonymousRegistration(Meteor.settings.GizwitsKey.jh.appId, user.username);
        var gARUserDataxf = GizwitsMethods.AnonymousRegistration(Meteor.settings.GizwitsKey.xf.appId, user.username);
        if (gARUserData && gARUserDataxf) {
            User_data.upsert({uid: obj.user._id}, {
                $set: {
                    gizwitsUserData: gARUserData,
                    gizwitsUserDataXF: gARUserDataxf
                }
            });
        }

        //var oldUser = Meteor.call('getUserDataById', obj.user._id);
        //console.log("----------");
        //console.log(oldUser);
        //
        //if (oldUser) {
        //    var gizwitsData = Meteor.call('gizwitsPost',
        //        'login', {"username": oldUser.username, "password": oldUser.pwd},
        //        Meteor.settings.GizwitsKey.jh.appId);
        //    var gizwitsDataXF = Meteor.call('gizwitsPost',
        //        'login', {"username": oldUser.username, "password": oldUser.pwd},
        //        Meteor.settings.GizwitsKey.xf.appId);
        //    console.log(gizwitsData, '\n', gizwitsDataXF);
        //
        //    if (gizwitsData.error_code || gizwitsDataXF.error_code) {
        //        console.log("----giz----login----error----");
        //        throw new Error(500, "登录机制云失败！");
        //    } else {
        //        console.log("----giz----login----success----");
        //        console.log("----gizwitsData----" + gizwitsData);
        //        console.log("----gizwitsDataXF----" + gizwitsDataXF);
        //        User_data.update({uid: obj.user._id},
        //            {
        //                $set: {
        //                    gizwitsUserData: JSON.parse(gizwitsData),
        //                    gizwitsUserDataXF: JSON.parse(gizwitsDataXF)
        //                }
        //            }
        //        );
        //    }
        //} else {
        //    console.log('-------login-------error--------');
        //}
    });

}

Meteor.methods({

    getUserDataById: function (_id) {
        return User_data.findOne({uid: _id});
    },
    SendSMS: function (Phone) {
        //Meteor.call('sendSMS5m', )
    },
    //用户登录之前先调用此方法，确定是否再极致云有该用户
    LoginGizUserWithoutLocalUser: function (username, password) {

        console.log("----LoginGizUserWithoutLocalUser----start----")

        var gizwitsData = Meteor.call('gizwitsPost', 'login',
            {"username": username, "password": password}, Meteor.settings.GizwitsKey.jh.appId);
        var gizwitsDataXF = Meteor.call('gizwitsPost', 'login',
            {"username": username, "password": password}, Meteor.settings.GizwitsKey.xf.appId);

        if (gizwitsData.error_code || gizwitsDataXF.error_code) {
            console.log("----LoginGizUserWithoutLocalUser----error----\n", gizwitsData, "\n", gizwitsDataXF);
            throw new Meteor.Error(gizwitsData.error_message);
        } else {
            var user = User_data.findOne({username: username});
            if (!user) {
                Accounts.createUser({
                    username: username,
                    password: password,
                    passwordText: password
                });
                console.log("----giz----user----inserted----");
            }
        }
        console.log("----LoginGizUserWithoutLocalUser----end----\n", gizwitsData, "\n", gizwitsDataXF);
    },
    changePasswordGizwits: function (_id, oldpwd, newpwd) {
        var user = Meteor.call('getUserDataById', _id);
        console.log(user);
        if (!user) throw new Error('用户不存在', 500);
        var data = {
            "old_pwd": oldpwd,
            "new_pwd": newpwd
        }

        var appidjh = Meteor.settings.GizwitsKey.jh.appId;
        var appidxf = Meteor.settings.GizwitsKey.xf.appId;

        console.log("jg ---token--" + user.gizwitsUserData.token);
        var jhdevdata = JSON.parse(Meteor.call("gizwitsPut", "/users", data, user.gizwitsUserData.token, appidjh));
        var xfdevdata = JSON.parse(Meteor.call("gizwitsPut", "/users", data, user.gizwitsUserDataXF.token, appidxf));


        if (jhdevdata && jhdevdata.updatedAt || xfdevdata && xfdevdata.updatedAt) {
            console.log("newpwd=====" + newpwd);
            console.log("uid----" + _id);
            User_data.update({uid: _id}, {$set: {pwd: newpwd}});
            //return true;
        }
        else {
            throw new Error('修改密码失败！', 500)
        }

    },

    addCityInUsers: function (_id, city) {
        console.log('in addCityInUser:id;city' + _id + ';' + city);
        User_data.update({uid: _id}, {$set: {city: city}});
    },
    //用户找回密码 ，设置新密码，
    resetPasswordClient: function (phone, newpwd) {
        var user = Meteor.users.findOne({username: phone});
        if (user) {
            Accounts.setPassword(user._id, newpwd);
        } else {
            throw new Error("不存在此用户!");
        }

    },
});

/**
 @return {boolean}
 * @param mobileNumber
 */
checkUserName = function (mobileNumber) {

    var regEx = /^[0-9]{11}$/i;

    return regEx.test(mobileNumber);
}
