// Write your package code here!
GLOBALIZATION = {
    getPreferredLanguage: function (callback) {
        if(Meteor.isCordova){
            navigator.globalization.getPreferredLanguage(
                function (language) {
                    callback(null, language);
                },
                function (error) {
                    callback(new Meteor.Error("cordovaError", error));
                }
            );
        }else{
            callback(new Meteor.Error("Not in the cordova"));
        }
    }
}