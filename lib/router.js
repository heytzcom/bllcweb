//Router.configure({
//    layoutTemplate: 'globalLayout'
//});
Router.map(function () {
    this.route('homePage',{layoutTemplate:"layout"});
    this.route('login', {path: '/'});//登录
    this.route('register');//注册
    this.route('updatePwd');//修改密码
    this.route('findPassword');//找回密码

    this.route('brownIndex');//布朗首页

    this.route('addNewWind');//初始化
    this.route('addNewWind2');//连接网络
    this.route('addNewWind3');//设置
    this.route('addNewWind4');//完成

    this.route('deviceControl');//设备控制
    this.route('deviceControlContent');//设备控制


    this.route('languagepage');//语言切换
    this.route('time');//定时
    this.route('strainer');//过滤网
    this.route('weather');//天气
    this.route('weatherStations');//气象站
    this.route('citySelector');//城市列表
    this.route('deviceManage');//设备管理
    this.route('help');
    this.route('messagePush');
    this.route('newtrend');//新风页面


    this.route('test');

    this.route('weixinDeviceControl1', {
        path: '/bind1/:openid',
        layoutTemplate:null,
        action: function () {
            Session.set('loginIn','start');
            var openid = this.params.openid;
            Meteor.subscribe('openid', openid);
            Session.set('openid', openid);
            console.log('before:'+Meteor.userId())
            if(!Meteor.userId()){
                console.log('after');
                var openidObj = Openid.findOne({"openid":Session.get('openid')});
                console.log('openidObj:'+openidObj);
                if(openidObj){
                    Session.set('weixinBind',true);
                    console.log('username:'+openidObj.username);
                    console.log('password:'+openidObj.password);
                    Meteor.call('LoginGizUserWithoutLocalUser', openidObj.username, openidObj.password,
                        function (error) {
                            if (error) {
                                console.log('in error');
                                if(!Session.get("popvalue")){
                                    Session.set("popvalue",1);
                                    IonPopup.show({
                                        title: '温馨提示',
                                        template: '手机号码或密码验证不通过',
                                        buttons: [{
                                            text: '关闭',
                                            type: 'button-positive',
                                            onTap: function () {
                                                Session.set("popvalue",null)
                                                IonPopup.close();
                                            }
                                        }]
                                    });
                                }
                            } else {
                                console.log('in else');
                                Meteor.loginWithPassword(openidObj.username, openidObj.password, function (error) {
                                        if (error) {
                                            console.log(error);
                                            Session.set('loginIn','error');
                                        } else {
                                            //Router.go('/brownIndex');
                                            console.log('Meteor.userId():'+Meteor.userId());
                                            //Meteor.subscribe('user_data', Meteor.userId());
                                            console.log('login success');
                                            //Meteor.subscribe('weathers', Meteor.userId());
                                            //Meteor.subscribe('modeInformation');
                                            //Meteor.subscribe('devices', Meteor.userId());
                                            //Meteor.subscribe('device_name', Meteor.userId());
                                            Session.set('username',openidObj.username);
                                            Session.set('loginIn','success');
                                            Meteor.subscribe('devices', Meteor.userId())

                                        }
                                    }
                                );
                            }
                        }
                    );
                }
                if(Meteor.userId()){
                    this.render();
                }else{
                    Meteor.setTimeout(function(){
                        if(!Meteor.userId()){
                            Session.set('notBinding','notBinding');
                            Router.go("/appBinding/" + Session.get('openid'));
                        }
                    },3000)
                }
            }else{
                Session.set('loginIn','success');
                this.render();
            }
        }
    });//设备控制

    this.route('weixinDeviceControl2', {
        path: '/bind2/:openid',
        layoutTemplate:null,
        action: function () {
            Session.set('loginIn','start');
            var openid = this.params.openid;
            Meteor.subscribe('openid', openid);
            Session.set('openid', openid);
            //if (openid) {
            //    Session.set('openid', openid);
            //    var o = Openid.findOne({"openid":openid});
            //    console.log('db:' + o);
            //    //if (o) {
            //    //    Router.go("/bind1/" + openid);
            //    //}
            //}
            console.log('Session.get(openid):'+Session.get('openid'));
            if(!Meteor.userId()) {
                var openidObj = Openid.findOne({"openid": Session.get('openid')});
                console.log('openidObj:' + openidObj);
                if (openidObj) {
                    Session.set('weixinBind', true);
                    console.log('username:' + openidObj.username);
                    console.log('password:' + openidObj.password);
                    Meteor.call('LoginGizUserWithoutLocalUser', openidObj.username, openidObj.password,
                        function (error) {
                            if (error) {
                                console.log('in error');
                                if (!Session.get("popvalue")) {
                                    Session.set("popvalue", 1);
                                    IonPopup.show({
                                        title: '温馨提示',
                                        template: '手机号码或密码验证不通过',
                                        buttons: [{
                                            text: '关闭',
                                            type: 'button-positive',
                                            onTap: function () {
                                                Session.set("popvalue", null)
                                                IonPopup.close();
                                            }
                                        }]
                                    });
                                }
                            } else {
                                console.log('in else');
                                console.log('openidObj.username:'+openidObj.username);
                                console.log('openidObj.password:'+openidObj.password);
                                Meteor.loginWithPassword(openidObj.username, openidObj.password, function (error) {
                                        if (error) {
                                            console.log('error:' + error)
                                            Session.set('loginIn', 'error');
                                        } else {
                                            //Router.go('/brownIndex');
                                            console.log('Meteor.userId():' + Meteor.userId());
                                            console.log('login success');
                                            Session.set('username',openidObj.username);
                                            Session.set('loginIn', 'success');
                                            Meteor.subscribe('devices', Meteor.userId())
                                        }
                                    }
                                );
                            }
                        }
                    );
                }
                if(Meteor.userId()){
                    this.render();
                }else{
                    Meteor.setTimeout(function(){
                        if(!Meteor.userId()){
                            Session.set('notBinding','notBinding');
                            Router.go("/appBinding/" + Session.get('openid'));
                        }
                    },3000)
                }
            }else{
                Session.set('loginIn','success');
                this.render();
            }
        }
    });//设备控制


    this.route('appBinding', {
        path: '/appBinding/:openid',
        layoutTemplate:null,
        //data: function () {
        //    return Openid.findOne(this.params.openid);
        //},

        action: function () {
            var openid = this.params.openid;
            Meteor.subscribe('openid', openid);
            if (openid) {
                Session.set('openid', openid);
                var o = Openid.findOne({"openid":openid});
                console.log('db:' + o);
                if (o) {
                    //Router.go("/bind1/" + openid);
                    Session.set('isBinding',true);
                }
            }
            this.render();
        }
    });//App绑定
});

WeixinDeviceControl1Controller = RouteController.extend({
    onBeforeAction: function () {
        console.log('----onBeforeAction----DeviceControl1----');
        console.log('Meteor.userId():'+Meteor.userId());
        Meteor.subscribe('weathers', Meteor.userId());
        Meteor.subscribe('modeInformation');
        var handel =Meteor.subscribe('devices', Meteor.userId());
        Meteor.subscribe('device_name', Meteor.userId());
        this.next();
    }
});

WeixinDeviceControl2Controller = RouteController.extend({
    onBeforeAction: function () {
        console.log('----onBeforeAction----DeviceControl2----');
        console.log('Meteor.userId()==='+Meteor.userId());
        Meteor.subscribe('weathers', Meteor.userId());
        Meteor.subscribe('modeInformation');
        var handel=   Meteor.subscribe('devices', Meteor.userId());
        Meteor.subscribe('device_name', Meteor.userId());
        this.next();
    }
});

WeatherController = RouteController.extend({
    onBeforeAction: function () {
        console.log('----onBeforeAction----Weather----');
        Meteor.subscribe('weathers', Meteor.userId());
        Meteor.subscribe('devices', Meteor.userId());
        this.next();
    }
});

DeviceControlController = RouteController.extend({
    onBeforeAction: function () {
        console.log('----onBeforeAction----DeviceControl----');
        Meteor.subscribe('weathers', Meteor.userId());
        Meteor.subscribe('modeInformation');
       var handel =Meteor.subscribe('devices', Meteor.userId());
        Meteor.subscribe('device_name', Meteor.userId());
        this.next();

    }
});

NewtrendController = RouteController.extend({
    onBeforeAction: function () {
        console.log('----onBeforeAction----Newtrend----');
        Meteor.subscribe('weathers', Meteor.userId());
        Meteor.subscribe('modeInformation');
     var handel=   Meteor.subscribe('devices', Meteor.userId());
        Meteor.subscribe('device_name', Meteor.userId());

           this.next();

    }
});

StrainerController = RouteController.extend({
    onBeforeAction: function () {
        console.log('----onBeforeAction----Strainer----');
        Meteor.subscribe('devices', Meteor.userId());
        this.next();
    }
});

DeviceManageController = RouteController.extend({
    onBeforeAction: function () {
        console.log('----onBeforeAction----DeviceManage----');
        Meteor.subscribe('device_name', Meteor.userId());
        Meteor.subscribe('devices', Meteor.userId());
        this.next();
    }
});

BrownIndexController = RouteController.extend({
    onBeforeAction: function () {
        console.log('----onBeforeAction----BrownIndex----');
        Meteor.subscribe('devices', Meteor.userId());
        Meteor.subscribe('user_data', Meteor.userId());
        this.next();
    }
});

TimeController = RouteController.extend({
    onBeforeAction: function () {
        console.log('----onBeforeAction----Time----');
        Meteor.subscribe('time_set', Meteor.userId());
        this.next();
    }
});

WeatherStationsController = RouteController.extend({
    onBeforeAction: function () {
        console.log('----onBeforeAction----weatherStations----');
        Meteor.subscribe('devices', Meteor.userId());
        this.next();
    }
});

