/**
 * Created by pcbeta on 15-3-29.
 */

if (Meteor.isClient) {
    Meteor.methods({
        loadingPage: function() {
           IonLoading.show({
               duration:30000,
               backdrop:true
           });
        },
        loadingdevicepromet: function(){
            IonLoading.show({
                customTemplate:'<h4>设备连接中...</h4>',
                duration:10000
            })
        },
        loadingControlFailed: function(str){
            IonLoading.show({
                customTemplate: str,
                duration: 3000
            })
        }

    })
}