/**
 * Created by vproUsr on 15-3-18.
 */
var DEVICE_LIST_KEY = "deviceArr";
var DEVICE_DID_KEY = "currentDid";
var DEVICE_MODE_KEY = "currentMode";
var DEVICE_PRODUCT_KEY = Meteor.settings.GizwitsKey.jh.productKey;
var DEVICE_MAC_KEY = "currentmac";


function reNewDevData(deviceArr) {
    for (var i = 0; i < deviceArr.length; i++) {
        Meteor.call("getDevdataLatest", Meteor.userId(), deviceArr[i].did);
    }
}

Template.deviceControl.rendered = function () {
    Meteor.subscribe("device_name", Meteor.userId());
    Meteor.subscribe("modeInformation");
    Meteor.subscribe("weathers", Meteor.userId());
    $("body").attr("class", "homePage");
    Session.set("cityselect_togo", 1);
    this.subscribe('controls', Meteor.userId());

    this.subscribe('devices', Meteor.userId(), function () {
        Session.set('controlState', false);
        var deviceArr = Devices.find({
            uid: Meteor.userId(),
            productkey: DEVICE_PRODUCT_KEY
        }).fetch();
        console.log(deviceArr);
        Session.set(DEVICE_LIST_KEY, deviceArr);

        //did
        var currDid = deviceArr[0].did;
        Session.set(DEVICE_DID_KEY, currDid);
        console.log("device-did-----" + Session.get(DEVICE_DID_KEY));
        console.log(Session.get(DEVICE_DID_KEY));

        //mac
        var currMac = deviceArr[0].mac;
        Session.set(DEVICE_MAC_KEY, currMac);
        console.log("device-mac===" + Session.get(DEVICE_MAC_KEY));

        //is_online
        var isonline = deviceArr[0].is_online;
        Session.set("deviceonline", isonline);
        var currentAttr = deviceArr[0].attr;
        console.log(currentAttr);
        if (currentAttr && currentAttr.Mode && isonline) {
            Session.set(DEVICE_MODE_KEY, Number(currentAttr.Mode));
            console.log(" Session.set(DEVICE_MODE_KEY, Number(currentAttr.Mode))==" + Session.get(DEVICE_MODE_KEY));
        } else {
            Session.set(DEVICE_MODE_KEY, 0);
            console.log(" Session.set(DEVICE_MODE_KEY, Number(currentAttr.Mode))==" + Session.get(DEVICE_MODE_KEY));
        }
        console.log(Session.get(DEVICE_LIST_KEY));
        console.log("-------------------")

        reNewDevData(Session.get(DEVICE_LIST_KEY));

        Session.set('deviceTime_id', Meteor.setInterval(function () {
            reNewDevData(Session.get(DEVICE_LIST_KEY));
        }, 4 * 1000));

    });


};

Template.deviceControl.helpers({
    currentDeviceName: function () {
        var currDevice = Device_name.findOne({did: Session.get(DEVICE_DID_KEY)})
        if (currDevice) {
            return currDevice.deviceName;
        }
    }
});

Template.deviceControl.events({
    "click .currentD": function (event, template) {
        console.log('/----选中的mac', this.did)
        Session.set(DEVICE_DID_KEY, this.did);
    }
});

Template.deviceControl.helpers({
    device: function () {
        var devArr = Devices.find({uid: Meteor.userId(), productkey: DEVICE_PRODUCT_KEY}).fetch();
        console.log('/----设备数据', devArr);
        for (var i = 0; i < devArr.length; i++) {
            var dev = Device_name.findOne({uid: Meteor.userId(), did: devArr[i].did});

            if (dev) {

                devArr[i].type = Meteor.settings.GizwitsKey.jh.name;
                devArr[i].deviceName = dev.deviceName;

            }
        }
        console.log("/----", devArr);
        return devArr;
    },
    weatherData: function () {
        var cityWeather = Weathers.find().fetch();
        var cityWeatherNew = cityWeather[cityWeather.length - 1];
        return cityWeatherNew;
    },
    deviceInfo: function () {
        //var did = this.did || Devices.findOne({uid: Meteor.userId(), productkey: DEVICE_PRODUCT_KEY}).did;
        var did = Session.get(DEVICE_DID_KEY);
        var deviceInfo = Devices.findOne({uid: Meteor.userId(), did: did});
        console.log('/----选中设备的参数', deviceInfo)
        if (deviceInfo && deviceInfo.attr) {
            if (deviceInfo.attr.Temp) {
                deviceInfo.attr.Temp = deviceInfo.attr.Temp.toString().split(".")[0];
                deviceInfo.attr.Hum = deviceInfo.attr.Hum;
                deviceInfo.attr.PM25 = deviceInfo.attr.PM25 >= 999 ? 999 : deviceInfo.attr.PM25
            }
            var isonline = Session.get("deviceonline");
            //TODO 时时获取当前状态
            if (deviceInfo.attr.Mode && isonline) {
                Session.set(DEVICE_MODE_KEY, Number(deviceInfo.attr.Mode));
            }
        }
        //
        //console.log(deviceInfo);
        return deviceInfo;
    },

    getControlImgSrc: function (str) {
        var currentMode = Session.get(DEVICE_MODE_KEY);

        var currModeObj = ModeInformation.findOne({mode: Number(currentMode), product: "jh"});
        console.log('/------A', currentMode, currModeObj);
        if (currModeObj) {
            return currModeObj[str];
        } else {
            return
        }
    }
});

Template.deviceControl.events({
    'click .js-click-onoff': function (events, template) {
        if (Session.get('controlState')) return;
        var currentMode = Session.get(DEVICE_MODE_KEY);
        if (currentMode != 0) {
            Session.set(DEVICE_MODE_KEY, 0);
        } else {
            Session.set(DEVICE_MODE_KEY, 3);
        }
        console.log("device-did-----" + Session.get(DEVICE_DID_KEY));
        console.log("device-mode----" + Session.get(DEVICE_MODE_KEY));
        Session.set('controlState', true);

        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY),
            Session.get(DEVICE_MODE_KEY), function (err) {
                if (err) {
                    Session.set(DEVICE_MODE_KEY, currentMode);
                }
                Session.set('controlState', false);
            });
    },

    'click .js-click-sleep': function (events, template) {

        if (Session.get('controlState'))return;
        var currentMode = Session.get(DEVICE_MODE_KEY);
        console.log("device-did-----" + Session.get(DEVICE_DID_KEY));
        console.log("device-mode----" + Session.get(DEVICE_MODE_KEY));
        if (currentMode != 0) {
            if (currentMode == 1) {
                Session.set(DEVICE_MODE_KEY, 0);
            } else {
                Session.set(DEVICE_MODE_KEY, 1);
            }
        } else {
            Session.set(DEVICE_MODE_KEY, 1);
        }
        Session.set('controlState', true);
        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY), Session.get(DEVICE_MODE_KEY), function (err) {
                if (err) {
                    Session.set(DEVICE_MODE_KEY, currentMode);
                }
                Session.set('controlState', false);
            });
    },

    'click .js-click-inter': function (events, template) {

        if (Session.get('controlState'))return;
        var currentMode = Session.get(DEVICE_MODE_KEY);
        console.log("device-did-----" + Session.get(DEVICE_DID_KEY));
        console.log("device-mode----" + Session.get(DEVICE_MODE_KEY));
        if (currentMode != 0) {
            if (currentMode == 2) {
                Session.set(DEVICE_MODE_KEY, 0);
            } else {
                Session.set(DEVICE_MODE_KEY, 2);
            }
        } else {
            Session.set(DEVICE_MODE_KEY, 2);
        }
        Session.set('controlState', true);
        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY),
            Session.get(DEVICE_MODE_KEY), function (err) {
                if (err) {
                    Session.set(DEVICE_MODE_KEY, currentMode);
                }
                Session.set('controlState', false);


            });
    },

    'click .js-click-wind': function (events, template) {

        if (Session.get('controlState'))return;
        var currentMode = Session.get(DEVICE_MODE_KEY);
        console.log("device-did-----" + Session.get(DEVICE_DID_KEY));
        console.log("device-mode----" + Session.get(DEVICE_MODE_KEY));
        if (currentMode != 0) {
            if (currentMode == 1 || currentMode == 2) {
                Session.set(DEVICE_MODE_KEY, 3);
            } else if (currentMode == 3) {
                Session.set(DEVICE_MODE_KEY, 4);
            } else if (currentMode == 4) {
                Session.set(DEVICE_MODE_KEY, 5);
            } else {
                Session.set(DEVICE_MODE_KEY, 0);
            }
        } else {
            Session.set(DEVICE_MODE_KEY, 3);
        }

        Session.set('controlState', true);
        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY), Session.get(DEVICE_MODE_KEY), function (err) {
                if (err) {
                    Session.set(DEVICE_MODE_KEY, currentMode);
                }
                Session.set('controlState', false);
            });
    }
});


Template.deviceControl.onDestroyed(function () {
    Meteor.clearInterval(Session.get("deviceTime_id"));
    Session.set("controlState", false);
});

