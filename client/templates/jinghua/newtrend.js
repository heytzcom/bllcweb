/**
 * Created by Tommy on 15/10/28.
 */
var DEVICE_LIST_KEY = "deviceArr";
var DEVICE_DID_KEY = "currentDid";
var DEVICE_MODE_KEY = "currentMode";
var DEVICE_PRODUCT_KEY = Meteor.settings.GizwitsKey.xf.productKey;
var DEVICE_NEWTREND_STATE="controlNewTrendState";
function reNewDevData(deviceArr) {
    for (var i=0; i<deviceArr.length; i++) {

        Meteor.call("getDevdataLatest", Meteor.userId(), deviceArr[i].did);
    }
}

Template.newtrend.onRendered(function () {
    $("body").attr("class", "homePage");
    Meteor.subscribe("device_name",Meteor.userId());
    Meteor.subscribe("modeInformation");
    Meteor.subscribe("weathers",Meteor.userId());
    Meteor.subscribe("devices",Meteor.userId());
    Session.set("cityselect_togo",2);
    this.subscribe('devices', Meteor.userId(), function () {

        var deviceArr = Devices.find({
            uid: Meteor.userId(),
            productkey: DEVICE_PRODUCT_KEY
        }).fetch();
        console.log('/----设备产品号',deviceArr);
        Session.set(DEVICE_LIST_KEY, deviceArr);
        if(!deviceArr[0]){
            Session.set(DEVICE_DID_KEY, false);
            Session.set('devonline',false);
            Session.set(DEVICE_MODE_KEY, false);
        }else{
            var currDid = deviceArr[0].did;
            Session.set(DEVICE_DID_KEY, currDid);
            console.log('/----deviceDID',Session.get(DEVICE_DID_KEY));
            //设备状态
            var isonline=deviceArr[0].is_online;
            Session.set('devonline',isonline);
            console.log(isonline+"======");

            var currentAttr = deviceArr[0].attr;
            if(currentAttr && currentAttr.Mode && isonline) {
                Session.set(DEVICE_MODE_KEY, Number(currentAttr.Mode));
            }else{
                Session.set(DEVICE_MODE_KEY, 5);
            }
        }

        reNewDevData(Session.get(DEVICE_LIST_KEY));
        Session.set('deviceTime_id_newTrend', Meteor.setInterval(function () {
            reNewDevData(Session.get(DEVICE_LIST_KEY));
        }, 4 * 1000));
    });
});
Template.newtrend.helpers({
    device: function () {
        var devArr = Devices.find({uid: Meteor.userId(), productkey: DEVICE_PRODUCT_KEY}).fetch();
        console.log('/----设备数据',devArr);
        for (var i=0; i<devArr.length; i++) {
            var dev = Device_name.findOne({uid: Meteor.userId(), did: devArr[i].did});

            if (dev) {
                    devArr[i].type = Meteor.settings.GizwitsKey.xf.name;
                    devArr[i].deviceName = dev.deviceName;
            }
        }
        console.log("/----",devArr);
        return devArr;
    },
    currentXFDeviceName: function() {
        var currDevice = Device_name.findOne({did: Session.get(DEVICE_DID_KEY)})
        if(currDevice) {
            return  currDevice.deviceName;
        }
    },
    getControlImgSrc: function(str) {
        var currentMode = Session.get(DEVICE_MODE_KEY);

        var modeInfo= ModeInformation.findOne({mode:currentMode, product: "xf"});
        //console.log("mode---",modeInfo.mode);
        if (modeInfo) {
            return modeInfo[str];
        } else {
            return;
        }
    },

    deviceInfo: function () {
        //var did = this.did || Devices.findOne({uid: Meteor.userId(),productkey: DEVICE_PRODUCT_KEY}).did;
        var did = Session.get(DEVICE_DID_KEY);
        var deviceInfo = Devices.findOne({uid: Meteor.userId(), did: did});
        if (deviceInfo && deviceInfo.attr) {

            if (deviceInfo.attr.Temp) {
                deviceInfo.attr.Temp = deviceInfo.attr.Temp.toString().split(".")[0];
                deviceInfo.attr.Filter = deviceInfo.attr.Filter;

            }
            var devisonline= Session.get('devonline')
            if(deviceInfo.attr.Mode && devisonline)
            {
                Session.set(DEVICE_MODE_KEY,Number(deviceInfo.attr.Mode));
            }
        }
        return deviceInfo;
    },

    weatherData: function () {
        var cityWeather = Weathers.find().fetch();
        var cityWeatherNew = cityWeather[cityWeather.length - 1];
        return cityWeatherNew;
    },

    getWeatherImgSrc: function (weatherStr) {
        var imgSrc;
        if (weatherStr && weatherStr.match('/')) {
            var str = weatherStr.split("/")[0];
        } else {
            var str = weatherStr;
        }
        switch (str) {

            case "晴":
                imgSrc = "../image/newtrendweather/000.png";
                break;
            case "多云":
                imgSrc = "../image/newtrendweather/001.png";
                break;
            case "阴/多云":
                imgSrc = "../image/newtrendweather/001.png";
                break;
            case "阴":
                imgSrc = "../image/newtrendweather/002.png";
                break;
            case "阵雨":
                imgSrc = "../image/newtrendweather/003.png";
                break;
            case "雷阵雨":
                imgSrc = "../image/newtrendweather/004.png";
                break;
            case "雷阵雨伴有冰雹":
                imgSrc = "../image/newtrendweather/005.png";
                break;
            case "雨夹雪":
                imgSrc = "../image/newtrendweather/006.png";
                break;
            case "小雨":
                imgSrc = "../image/newtrendweather/007.png";
                break;
            case "中雨":
                imgSrc = "../image/newtrendweather/008.png";
                break;
            case "大雨":
                imgSrc = "../image/newtrendweather/009.png";
                break;
            case "暴雨":
                imgSrc = "../image/newtrendweather/010.png";
                break;
            case "大暴雨":
                imgSrc = "../image/newtrendweather/011.png";
                break;
            case "特大暴雨":
                imgSrc = "../image/newtrendweather/012.png";
                break;
            case "阵雪":
                imgSrc = "../image/newtrendweather/013.png";
                break;
            case "小雪":
                imgSrc = "../image/newtrendweather/014.png";
                break;
            case "中雪":
                imgSrc = "../image/newtrendweather/015.png";
                break;
            case "大雪":
                imgSrc = "../image/newtrendweather/016.png";
                break;
            case "暴雪":
                imgSrc = "../image/newtrendweather/017.png";
                break;
            case"特大暴雪":
                imgSrc = "../image/newtrendweather/032.png";
                break;
            case "雾":
                imgSrc = "../image/newtrendweather/018.png";
                break;
            case "冻雨":
                imgSrc = "../image/newtrendweather/019.png";
                break;
            case "沙尘暴":
                imgSrc = "../image/newtrendweather/020.png";
                break;
            case "小到中雨":
                imgSrc = "../image/newtrendweather/021.png";
                break;
            case "中到大雨":
                imgSrc = "../image/newtrendweather/022.png";
                break;
            case "大到暴雨":
                imgSrc = "../image/newtrendweather/023.png";
                break;
            case "暴雨到大暴雨":
                imgSrc = "../image/newtrendweather/024.png";
                break;
            case "大暴雨到特大暴雨":
                imgSrc = "../image/newtrendweather/025.png";
                break;
            case "小到中雪":
                imgSrc = "../image/newtrendweather/026.png";
                break;
            case "中到大雪":
                imgSrc = "../image/newtrendweather/027.png";
                break;
            case "大到暴雪":
                imgSrc = "../image/newtrendweather/028.png";
                break;
            case "浮尘":
                imgSrc = "../image/newtrendweather/029.png";
                break;
            case "扬沙":
                imgSrc = "../image/newtrendweather/030.png";
                break;
            case "强沙尘暴":
                imgSrc = "../image/newtrendweather/031.png";
                break;
            case "霾":
                imgSrc = "../image/newtrendweather/053.png";
                break;
            default:
                imgSrc = "../image/newtrendweather/099.png";
        }
        return imgSrc;
    },

    TempNew:function(Temp){
        if(Temp){
            return  (Number(Temp)*0.1).toFixed(1);
        }
        return;
    },
    HumNew:function(Hum){
        if(Hum){
            return parseInt(Hum.substr(22,2).toString(),10);
        }
        return;
    }
});

Template.newtrend.events({

    'click .js-click-onoff': function() {
        if(Session.get(DEVICE_NEWTREND_STATE)) return;
        var modeNum = Session.get(DEVICE_MODE_KEY);
        if (modeNum == 4) {
            Session.set(DEVICE_MODE_KEY, 5);
        } else {
            Session.set(DEVICE_MODE_KEY, 4);
        }
        Session.set(DEVICE_NEWTREND_STATE,true);
        console.log("//-------------mode------:"+Session.get(DEVICE_MODE_KEY));
        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY), Session.get(DEVICE_MODE_KEY),function(err){
                if(err){
                    Session.set(DEVICE_MODE_KEY,modeNum);
                    IonLoading.show({
                        //customTemplate: "<h4>控制失败</h4><p>"+err.message+"</p>",
                        customTemplate: "<h4>"+TAPi18n.__("newtrend_newTrendContent_events_click_onoff_custom")+"</h4>",
                        duration: 300
                    })
                }
                Session.set(DEVICE_NEWTREND_STATE,false);
            });

    },

    'click .js-click-inter': function() {
        if(Session.get(DEVICE_NEWTREND_STATE)) return;
        var modeNum = Session.get(DEVICE_MODE_KEY);
        if (modeNum == 3) {
            Session.set(DEVICE_MODE_KEY, 5);
        } else {
            Session.set(DEVICE_MODE_KEY, 3);
        }
        Session.set(DEVICE_NEWTREND_STATE,true);
        console.log("//-------------mode------:"+Session.get(DEVICE_MODE_KEY));
        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY), Session.get(DEVICE_MODE_KEY),function(err){
                if(err){
                    Session.set(DEVICE_MODE_KEY,modeNum);
                    IonLoading.show({
                        customTemplate: "<h4>"+TAPi18n.__("newtrend_newTrendContent_events_click_inter_custom")+"</h4>",
                        duration: 300
                    })
                }
                Session.set(DEVICE_NEWTREND_STATE,false);
            });
    },

    'click .js-click-low': function() {
        if(Session.get(DEVICE_NEWTREND_STATE)) return;
        var modeNum = Session.get(DEVICE_MODE_KEY);
        if (modeNum == 2) {
            Session.set(DEVICE_MODE_KEY, 5);
        } else {
            Session.set(DEVICE_MODE_KEY,2);
        }
        Session.set(DEVICE_NEWTREND_STATE,true);
        console.log("//-------------mode------:"+Session.get(DEVICE_MODE_KEY));
        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY), Session.get(DEVICE_MODE_KEY),function(err){
                if(err){
                    Session.set(DEVICE_MODE_KEY,modeNum);
                    IonLoading.show({
                        customTemplate: "<h4>"+TAPi18n.__("newtrend_newTrendContent_events_click_low_custom")+"</h4>",
                        duration: 300
                    })
                }
                Session.set(DEVICE_NEWTREND_STATE,false);
            });
    },

    'click .js-click-high': function() {
        if(Session.get(DEVICE_NEWTREND_STATE)) return;
        var modeNum = Session.get(DEVICE_MODE_KEY);
        if (modeNum == 1) {
            Session.set(DEVICE_MODE_KEY, 5);
        } else {
            Session.set(DEVICE_MODE_KEY, 1);
        }
        Session.set(DEVICE_NEWTREND_STATE,true);
        console.log("//-------------mode------:"+Session.get(DEVICE_MODE_KEY));
        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY), Session.get(DEVICE_MODE_KEY),function(err){
                if(err){
                    Session.set(DEVICE_MODE_KEY,modeNum);
                    IonLoading.show({
                        customTemplate: "<h4>"+TAPi18n.__("newtrend_newTrendContent_events_click_high_custom")+"</h4>",
                        duration: 300
                    })
                }
                Session.set(DEVICE_NEWTREND_STATE,false);
            });
    },
    "click .currentD": function (event,template) {
        console.log('/----选中的mac',this.did)
        Session.set(DEVICE_DID_KEY, this.did);
    }
});

