/**
 * Created by chendongdong on 15/6/1.
 */
Template.login.onRendered(function () {
    $("body").attr("class","login")
});

Template.login.events({
    'click #login-js':function(event,template){
        var userName = template.$('[id=username]').val();
        var password = template.$('[id=userPwd]').val();
        event.preventDefault();

        Meteor.loginWithPassword(userName, password, function (error) {
                if (error) {
                    alert("账号或密码错误");
                } else {
                    Router.go('/homePage');
                }
            }
        );
    }
});