/**
 * Created by Tommy on 15/10/27.
 */
if (!Meteor.userId()) {
    Router.go("/");
}
var CURRENTDeviceType = "currentDeviceType";
var DEVICE_LIST_KEY = "deviceArr";
var DEVICE_DID_KEY = "currentDid";
var DEVICE_MODE_KEY = "currentMode";
var DEVICE_PRODUCT_JH_KEY = Meteor.settings.GizwitsKey.jh.productKey;
var DEVICE_PRODUCT_XF_KEY = Meteor.settings.GizwitsKey.xf.productKey;
var DEVICE_MAC_KEY = "currentmac";
function reNewDevData(deviceArr) {
    for (var i = 0; i < deviceArr.length; i++) {
        Meteor.call("getDevdataLatest", Meteor.userId(), deviceArr[i].did);
    }
}

Template.homePage.rendered = function () {
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
};
Template.homePage.onRendered(function () {
    $("body").attr("class", "homePage");
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
    Meteor.subscribe("devices", Meteor.userId());
    Meteor.call("getDeviceList", null, null, Meteor.userId());
    Session.set(CURRENTDeviceType, "jh");
    var user = User_data.findOne({uid: Meteor.userId()});
    if (user && !user.city) {
        Meteor.call('addCityInUsers', Meteor.userId(), TAPi18n.__("brownIndex_onRendered_city"));
    }
});
Template.homePage.rendered = function () {
    Meteor.subscribe("device_name", Meteor.userId());
    Meteor.subscribe("modeInformation");
    Meteor.subscribe("user_data");
    Meteor.subscribe("weathers", Meteor.userId());
    $("body").attr("class", "homePage");
    Session.set("cityselect_togo", 1);
    this.subscribe('controls', Meteor.userId());
    if (Session.get(CURRENTDeviceType) == "jh") {
        this.subscribe('devices', Meteor.userId(), function () {
            Session.set('controlState', false);
            var deviceArr = Devices.find({
                uid: Meteor.userId(),
                productkey: DEVICE_PRODUCT_JH_KEY
            }).fetch();
            console.log(deviceArr);
            Session.set(DEVICE_LIST_KEY, deviceArr);

            //did
            var currDid = deviceArr[0].did;
            Session.set(DEVICE_DID_KEY, currDid);
            console.log("device-did-----" + Session.get(DEVICE_DID_KEY));
            console.log(Session.get(DEVICE_DID_KEY));

            //mac
            var currMac = deviceArr[0].mac;
            Session.set(DEVICE_MAC_KEY, currMac);
            console.log("device-mac===" + Session.get(DEVICE_MAC_KEY));

            //is_online
            var isonline = deviceArr[0].is_online;
            Session.set("deviceonline", isonline);
            var currentAttr = deviceArr[0].attr;
            console.log(currentAttr);
            if (currentAttr && currentAttr.Mode && isonline) {
                Session.set(DEVICE_MODE_KEY, Number(currentAttr.Mode));
                console.log(" Session.set(DEVICE_MODE_KEY, Number(currentAttr.Mode))==" + Session.get(DEVICE_MODE_KEY));
            } else {
                Session.set(DEVICE_MODE_KEY, 0);
                console.log(" Session.set(DEVICE_MODE_KEY, Number(currentAttr.Mode))==" + Session.get(DEVICE_MODE_KEY));
            }
            console.log(Session.get(DEVICE_LIST_KEY));
            console.log("-------------------")

            reNewDevData(Session.get(DEVICE_LIST_KEY));

            Session.set('deviceTime_id', Meteor.setInterval(function () {
                reNewDevData(Session.get(DEVICE_LIST_KEY));
            }, 4 * 1000));

        });
    } else {
        this.subscribe('devices', Meteor.userId(), function () {

            var deviceArr = Devices.find({
                uid: Meteor.userId(),
                productkey: DEVICE_PRODUCT_XF_KEY
            }).fetch();
            console.log('/----设备产品号', deviceArr);
            Session.set(DEVICE_LIST_KEY, deviceArr);
            if (!deviceArr[0]) {
                Session.set(DEVICE_DID_KEY, false);
                Session.set('devonline', false);
                Session.set(DEVICE_MODE_KEY, false);
            } else {
                var currDid = deviceArr[0].did;
                Session.set(DEVICE_DID_KEY, currDid);
                console.log('/----deviceDID', Session.get(DEVICE_DID_KEY));
                //设备状态
                var isonline = deviceArr[0].is_online;
                Session.set('devonline', isonline);
                console.log(isonline + "======");

                var currentAttr = deviceArr[0].attr;
                if (currentAttr && currentAttr.Mode && isonline) {
                    Session.set(DEVICE_MODE_KEY, Number(currentAttr.Mode));
                } else {
                    Session.set(DEVICE_MODE_KEY, 5);
                }
            }

            reNewDevData(Session.get(DEVICE_LIST_KEY));
            Session.set('deviceTime_id_newTrend', Meteor.setInterval(function () {
                reNewDevData(Session.get(DEVICE_LIST_KEY));
            }, 4 * 1000));
        });
    }
};
Template.homePage.helpers({
    loginUserName: function () {
        return User_data.findOne({uid: Meteor.userId()}).username;
    },
    device: function () {
        if (Session.get(CURRENTDeviceType) == "jh") {
            var devArr = Devices.find({uid: Meteor.userId(), productkey: DEVICE_PRODUCT_JH_KEY}).fetch();
            console.log('/----设备数据', devArr);
            for (var i = 0; i < devArr.length; i++) {
                var dev = Device_name.findOne({uid: Meteor.userId(), did: devArr[i].did});

                if (dev) {

                    devArr[i].type = Meteor.settings.GizwitsKey.jh.name;
                    devArr[i].deviceName = dev.deviceName;

                }
            }
        } else {
            var devArr = Devices.find({uid: Meteor.userId(), productkey: DEVICE_PRODUCT_XF_KEY}).fetch();
            console.log('/----设备数据', devArr);
            for (var i = 0; i < devArr.length; i++) {
                var dev = Device_name.findOne({uid: Meteor.userId(), did: devArr[i].did});

                if (dev) {
                    devArr[i].type = Meteor.settings.GizwitsKey.xf.name;
                    devArr[i].deviceName = dev.deviceName;
                }
            }
        }
        console.log("/----", devArr);
        return devArr;
    },
    currentDeviceName: function () {
        var currDevice = Device_name.findOne({did: Session.get(DEVICE_DID_KEY)});
        if (currDevice) {
            return currDevice.deviceName;
        }
    },
    weatherData: function () {
        var cityWeather = Weathers.find().fetch();
        var cityWeatherNew = cityWeather[cityWeather.length - 1];
        if (cityWeatherNew) {
            return cityWeatherNew;
        }
    },
    deviceInfo: function () {
        var did;
        if (Session.get(CURRENTDeviceType) == "jh") {
            did = this.did || Devices.findOne({uid: Meteor.userId(), productkey: DEVICE_PRODUCT_JH_KEY}).did;
        } else {
            did = this.did || Devices.findOne({uid: Meteor.userId(), productkey: DEVICE_PRODUCT_XF_KEY}).did;
        }
        var deviceInfo = Devices.findOne({uid: Meteor.userId(), did: did});
        console.log('/----选中设备的参数', deviceInfo)
        if (deviceInfo && deviceInfo.attr) {
            if (deviceInfo.attr.Temp) {
                deviceInfo.attr.Temp = deviceInfo.attr.Temp.toString().split(".")[0];
                deviceInfo.attr.Hum = deviceInfo.attr.Hum;
                deviceInfo.attr.PM25 = deviceInfo.attr.PM25 >= 999 ? 999 : deviceInfo.attr.PM25
            }
            var isonline = Session.get("deviceonline");
            //TODO 时时获取当前状态
            if (deviceInfo.attr.Mode && isonline) {
                Session.set(DEVICE_MODE_KEY, Number(deviceInfo.attr.Mode));
            }
        }

        return deviceInfo;
    },

    getControlImgSrc: function (str) {
        var currentMode = Session.get(DEVICE_MODE_KEY);

        var currModeObj = ModeInformation.findOne({mode: Number(currentMode), product: Session.get(CURRENTDeviceType)});
        console.log('/------A', currentMode, currModeObj);
        if (currModeObj) {
            return currModeObj[str];
        } else {
            return
        }
    },

    getWeatherImgSrc: function (weatherStr) {
        var imgSrc;
        if (weatherStr && weatherStr.match('/')) {
            var str = weatherStr.split("/")[0];
        } else {
            var str = weatherStr;
        }
        switch (str) {

            case "晴":
                imgSrc = "../image/newtrendweather/000.png";
                break;
            case "多云":
                imgSrc = "../image/newtrendweather/001.png";
                break;
            case "阴/多云":
                imgSrc = "../image/newtrendweather/001.png";
                break;
            case "阴":
                imgSrc = "../image/newtrendweather/002.png";
                break;
            case "阵雨":
                imgSrc = "../image/newtrendweather/003.png";
                break;
            case "雷阵雨":
                imgSrc = "../image/newtrendweather/004.png";
                break;
            case "雷阵雨伴有冰雹":
                imgSrc = "../image/newtrendweather/005.png";
                break;
            case "雨夹雪":
                imgSrc = "../image/newtrendweather/006.png";
                break;
            case "小雨":
                imgSrc = "../image/newtrendweather/007.png";
                break;
            case "中雨":
                imgSrc = "../image/newtrendweather/008.png";
                break;
            case "大雨":
                imgSrc = "../image/newtrendweather/009.png";
                break;
            case "暴雨":
                imgSrc = "../image/newtrendweather/010.png";
                break;
            case "大暴雨":
                imgSrc = "../image/newtrendweather/011.png";
                break;
            case "特大暴雨":
                imgSrc = "../image/newtrendweather/012.png";
                break;
            case "阵雪":
                imgSrc = "../image/newtrendweather/013.png";
                break;
            case "小雪":
                imgSrc = "../image/newtrendweather/014.png";
                break;
            case "中雪":
                imgSrc = "../image/newtrendweather/015.png";
                break;
            case "大雪":
                imgSrc = "../image/newtrendweather/016.png";
                break;
            case "暴雪":
                imgSrc = "../image/newtrendweather/017.png";
                break;
            case"特大暴雪":
                imgSrc = "../image/newtrendweather/032.png";
                break;
            case "雾":
                imgSrc = "../image/newtrendweather/018.png";
                break;
            case "冻雨":
                imgSrc = "../image/newtrendweather/019.png";
                break;
            case "沙尘暴":
                imgSrc = "../image/newtrendweather/020.png";
                break;
            case "小到中雨":
                imgSrc = "../image/newtrendweather/021.png";
                break;
            case "中到大雨":
                imgSrc = "../image/newtrendweather/022.png";
                break;
            case "大到暴雨":
                imgSrc = "../image/newtrendweather/023.png";
                break;
            case "暴雨到大暴雨":
                imgSrc = "../image/newtrendweather/024.png";
                break;
            case "大暴雨到特大暴雨":
                imgSrc = "../image/newtrendweather/025.png";
                break;
            case "小到中雪":
                imgSrc = "../image/newtrendweather/026.png";
                break;
            case "中到大雪":
                imgSrc = "../image/newtrendweather/027.png";
                break;
            case "大到暴雪":
                imgSrc = "../image/newtrendweather/028.png";
                break;
            case "浮尘":
                imgSrc = "../image/newtrendweather/029.png";
                break;
            case "扬沙":
                imgSrc = "../image/newtrendweather/030.png";
                break;
            case "强沙尘暴":
                imgSrc = "../image/newtrendweather/031.png";
                break;
            case "霾":
                imgSrc = "../image/newtrendweather/053.png";
                break;
            default:
                imgSrc = "../image/newtrendweather/099.png";
        }
        return imgSrc;
    },
    getWindString: function (wind) {
        return wind.split('风')[0] + '风';
    },

    'getMonthDayString': function (datestring) {
        return datestring.substring(5, 10).replace('-', '/');
    },

    'getDateTitle': function (lastupdate) {
        var temp1 = jsonDateToSortString(lastupdate);
        // console.log(temp1);
        return stringToDateString(temp1, 0);
    },
    deviceType: function () {
        if (Session.get(CURRENTDeviceType) == "jh") {
            return true
        } else if (Session.get(CURRENTDeviceType) == "xf") {
            return false
        }
    },
    TempNew: function (Temp) {
        if (Temp) {
            return (Number(Temp) * 0.1).toFixed(1);
        }
        return;
    }
});
Template.homePage.events({
    "click #logout": function () {
        Meteor.logout(function (err) {
            if (!err) {
                console.log("loginout--success");

                Router.go("/");

            } else {
                console.log("loginout--error" + err);
            }
        });
    },
    "click .currentD": function (event, template) {
        console.log('/----选中的mac', this.did)
        Session.set(DEVICE_DID_KEY, this.did);
    },
    "click #jhImg": function () {

        //var deviceArr = Devices.find({
        //    uid: Meteor.userId(),
        //    productkey: Meteor.settings.GizwitsKey.jh.productKey
        //}).fetch();
        Session.set(CURRENTDeviceType, "jh");
    },

    "click #ktImg": function () {

    },

    "click #xfImg": function () {

        //var deviceArr = Devices.find({
        //    uid: Meteor.userId(),
        //    productkey: Meteor.settings.GizwitsKey.xf.productKey
        //}).fetch();
        Session.set(CURRENTDeviceType, "xf");
    },

    "click #dnnImg": function () {

    },
    'click .js-click-onoff': function (events, template) {
        if (Session.get('controlState')) return;
        var currentMode = Session.get(DEVICE_MODE_KEY);
        if (currentMode != 0) {
            Session.set(DEVICE_MODE_KEY, 0);
        } else {
            Session.set(DEVICE_MODE_KEY, 3);
        }
        console.log("device-did-----" + Session.get(DEVICE_DID_KEY));
        console.log("device-mode----" + Session.get(DEVICE_MODE_KEY));
        Session.set('controlState', true);

        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY),
            Session.get(DEVICE_MODE_KEY), function (err) {
                if (err) {
                    Session.set(DEVICE_MODE_KEY, currentMode);
                }
                Session.set('controlState', false);
            });
    },

    'click .js-click-sleep': function (events, template) {

        if (Session.get('controlState'))return;
        var currentMode = Session.get(DEVICE_MODE_KEY);
        console.log("device-did-----" + Session.get(DEVICE_DID_KEY));
        console.log("device-mode----" + Session.get(DEVICE_MODE_KEY));
        if (currentMode != 0) {
            if (currentMode == 1) {
                Session.set(DEVICE_MODE_KEY, 0);
            } else {
                Session.set(DEVICE_MODE_KEY, 1);
            }
        } else {
            Session.set(DEVICE_MODE_KEY, 1);
        }
        Session.set('controlState', true);
        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY), Session.get(DEVICE_MODE_KEY), function (err) {
                if (err) {
                    Session.set(DEVICE_MODE_KEY, currentMode);
                }
                Session.set('controlState', false);
            });
    },

    'click .js-click-inter': function (events, template) {

        if (Session.get('controlState'))return;
        var currentMode = Session.get(DEVICE_MODE_KEY);
        console.log("device-did-----" + Session.get(DEVICE_DID_KEY));
        console.log("device-mode----" + Session.get(DEVICE_MODE_KEY));
        if (currentMode != 0) {
            if (currentMode == 2) {
                Session.set(DEVICE_MODE_KEY, 0);
            } else {
                Session.set(DEVICE_MODE_KEY, 2);
            }
        } else {
            Session.set(DEVICE_MODE_KEY, 2);
        }
        Session.set('controlState', true);
        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY),
            Session.get(DEVICE_MODE_KEY), function (err) {
                if (err) {
                    Session.set(DEVICE_MODE_KEY, currentMode);
                }
                Session.set('controlState', false);


            });
    },

    'click .js-click-wind': function (events, template) {

        if (Session.get('controlState'))return;
        var currentMode = Session.get(DEVICE_MODE_KEY);
        console.log("device-did-----" + Session.get(DEVICE_DID_KEY));
        console.log("device-mode----" + Session.get(DEVICE_MODE_KEY));
        if (currentMode != 0) {
            if (currentMode == 1 || currentMode == 2) {
                Session.set(DEVICE_MODE_KEY, 3);
            } else if (currentMode == 3) {
                Session.set(DEVICE_MODE_KEY, 4);
            } else if (currentMode == 4) {
                Session.set(DEVICE_MODE_KEY, 5);
            } else {
                Session.set(DEVICE_MODE_KEY, 0);
            }
        } else {
            Session.set(DEVICE_MODE_KEY, 3);
        }

        Session.set('controlState', true);
        Meteor.call('deviceModeControl', Meteor.userId(),
            Session.get(DEVICE_DID_KEY), Session.get(DEVICE_MODE_KEY), function (err) {
                if (err) {
                    Session.set(DEVICE_MODE_KEY, currentMode);
                }
                Session.set('controlState', false);
            });
    },
    "click .navbar-toggle": function () {
        var aa = $("#js-bootstrap-offcanvas");
        if (aa.width() > 0) {
            aa.stop().animate({width: "0px"}, 300);
            $("#js-bootstrap-offcanvas").prop("class", "bavbarDisplays")
        } else {
            aa.stop().animate({width: "20%"}, 300);
            $("#js-bootstrap-offcanvas").prop("class", "bavbarDisplay")
        }

    }
});


Template.homePage.onDestroyed(function () {
    Meteor.clearInterval(Session.get("deviceTime_id"));
    Session.set("controlState", false);
});


/* json 日期转字符串 */
function jsonDataToString(dateStr) {
    if (dateStr.indexOf('T') != -1 && dateStr.indexOf('.') != -1) {
        dateStr = dateStr.replace('T', ' ').substring(0, dateStr.indexOf('.'));
    }
    return dateStr;
}


///* 转短字符串 */ （yyyy-MM-dd）
function jsonDateToSortString(dateStr) {
    if (!dateStr) {
        return dateStr;
    }
    if (dateStr.indexOf('T') != -1) {
        dateStr = dateStr.substring(0, dateStr.indexOf('T'));
    }
    return dateStr;
}

//	（yyyy-MM-dd）
function stringToDateString(str, num) {
    var arr = str.split("-");
    var dd = new Date();
    dd.setMonth(Number(arr[1]) - 1);
    dd.setDate(Number(arr[2]) + num);
    var ddStr = dd.getMonth() + 1 + "/" + dd.getDate();
    return ddStr;
}



