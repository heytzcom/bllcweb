/**
 * Created by pcbeta on 15-10-23.
 */
var displayKey = "displayKey";
Template.navbar.onRendered(function () {
    Meteor.subscribe("publishUser");
    Meteor.subscribe("clientUser");
    Session.set(displayKey,false);
    if(Session.get(displayKey)){
        $("#js-bootstrap-offcanvas").prop("class","bavbarDisplay")
    }else{
        $("#js-bootstrap-offcanvas").prop("class","bavbarDisplays")
    }
});
Template.navbar.rendered = function () {
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
};
Template.navbar.helpers({

});
Template.navbar.events({
    "click #closeRightBar": function () {
        $("#js-bootstrap-offcanvas").stop().animate({width: "0px"}, 300);
        $("#js-bootstrap-offcanvas").prop("class","bavbarDisplays")
    }
});