/**
 * Created by chendongdong on 15/8/12.
 */



Template.sidebar.onRendered(function () {
    $("body").attr("class","page-header-fixed page-quick-sidebar-over-content page-style-square")
    Layout.init();
    Metronic.init();
    QuickSidebar.init();
    Demo.init();
    Index.init();
    //Index.initDashboardDaterange();
    //Index.initJQVMAP(); // init index page's custom scripts
    //Index.initCalendar(); // init index page's custom scripts
    //Index.initCharts(); // init index page's custom scripts
    //Index.initChat();
    //Index.initMiniCharts();
});

Template.sidebar.events({
    "click li":function(event){
        $(".page-sidebar-menu").find('li').removeClass("start");
        $(".page-sidebar-menu").find('li').removeClass("active");
        $(".page-sidebar-menu").find('li').removeClass("open");
        $(event.currentTarget).addClass("start");
        $(event.currentTarget).addClass("active");
        $(event.currentTarget).addClass("open");
    }
});