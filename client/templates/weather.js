/**
 * Created by vproUsr on 15-3-19.
 */
Template.weather.onRendered(function () {
    $("body").attr("class","homePage");
    Meteor.subscribe("device_name");
    Meteor.subscribe("devices");
});
Template.weather.helpers({

    weatherData: function () {
        var cityWeather = Weathers.find().fetch();
        var cityWeatherNew = cityWeather[cityWeather.length - 1];
        return cityWeatherNew;
    },


    getWeatherImgSrc: function (weatherStr) {
        var imgSrc;
        if (weatherStr && weatherStr.match('/')) {
            var str = weatherStr.split("/")[0];
        } else {
            var str = weatherStr;
        }
        switch (str) {

            case "晴":
                imgSrc = "../image/newtrendweather/000.png";
                break;
            case "多云":
                imgSrc = "../image/newtrendweather/001.png";
                break;
            case "阴/多云":
                imgSrc = "../image/newtrendweather/001.png";
                break;
            case "阴":
                imgSrc = "../image/newtrendweather/002.png";
                break;
            case "阵雨":
                imgSrc = "../image/newtrendweather/003.png";
                break;
            case "雷阵雨":
                imgSrc = "../image/newtrendweather/004.png";
                break;
            case "雷阵雨伴有冰雹":
                imgSrc = "../image/newtrendweather/005.png";
                break;
            case "雨夹雪":
                imgSrc = "../image/newtrendweather/006.png";
                break;
            case "小雨":
                imgSrc = "../image/newtrendweather/007.png";
                break;
            case "中雨":
                imgSrc = "../image/newtrendweather/008.png";
                break;
            case "大雨":
                imgSrc = "../image/newtrendweather/009.png";
                break;
            case "暴雨":
                imgSrc = "../image/newtrendweather/010.png";
                break;
            case "大暴雨":
                imgSrc = "../image/newtrendweather/011.png";
                break;
            case "特大暴雨":
                imgSrc = "../image/newtrendweather/012.png";
                break;
            case "阵雪":
                imgSrc = "../image/newtrendweather/013.png";
                break;
            case "小雪":
                imgSrc = "../image/newtrendweather/014.png";
                break;
            case "中雪":
                imgSrc = "../image/newtrendweather/015.png";
                break;
            case "大雪":
                imgSrc = "../image/newtrendweather/016.png";
                break;
            case "暴雪":
                imgSrc = "../image/newtrendweather/017.png";
                break;
            case"特大暴雪":
                imgSrc = "../image/newtrendweather/032.png";
                break;
            case "雾":
                imgSrc = "../image/newtrendweather/018.png";
                break;
            case "冻雨":
                imgSrc = "../image/newtrendweather/019.png";
                break;
            case "沙尘暴":
                imgSrc = "../image/newtrendweather/020.png";
                break;
            case "小到中雨":
                imgSrc = "../image/newtrendweather/021.png";
                break;
            case "中到大雨":
                imgSrc = "../image/newtrendweather/022.png";
                break;
            case "大到暴雨":
                imgSrc = "../image/newtrendweather/023.png";
                break;
            case "暴雨到大暴雨":
                imgSrc = "../image/newtrendweather/024.png";
                break;
            case "大暴雨到特大暴雨":
                imgSrc = "../image/newtrendweather/025.png";
                break;
            case "小到中雪":
                imgSrc = "../image/newtrendweather/026.png";
                break;
            case "中到大雪":
                imgSrc = "../image/newtrendweather/027.png";
                break;
            case "大到暴雪":
                imgSrc = "../image/newtrendweather/028.png";
                break;
            case "浮尘":
                imgSrc = "../image/newtrendweather/029.png";
                break;
            case "扬沙":
                imgSrc = "../image/newtrendweather/030.png";
                break;
            case "强沙尘暴":
                imgSrc = "../image/newtrendweather/031.png";
                break;
            case "霾":
                imgSrc = "../image/newtrendweather/053.png";
                break;
            default:
                imgSrc = "../image/newtrendweather/099.png";
        }
        return imgSrc;
    },
    getWindString:function(wind){
        return  wind.split('风')[0]+'风';
    },

    'getMonthDayString': function (datestring) {
        return datestring.substring(5, 10).replace('-', '/');
    },

    'getDateTitle': function (lastupdate) {
        var temp1 = jsonDateToSortString(lastupdate);
       // console.log(temp1);
        return stringToDateString(temp1, 0);
    }


})


//Template.weather.onRendered(function () {
//
//    Meteor.subscribe('user_data',Meteor.userId(),function(){
//        var city=User_data.findOne({uid:Meteor.userId()}).city;
//      if(!city){
//          Router.go('/citySelector');
//      }
//    });
//});


/* json 日期转字符串 */
function jsonDataToString(dateStr) {
    if (dateStr.indexOf('T') != -1 && dateStr.indexOf('.') != -1) {
        dateStr = dateStr.replace('T', ' ').substring(0, dateStr.indexOf('.'));
    }
    return dateStr;
}


///* 转短字符串 */ （yyyy-MM-dd）
function jsonDateToSortString(dateStr) {
    if (!dateStr) {
        return dateStr;
    }
    if (dateStr.indexOf('T') != -1) {
        dateStr = dateStr.substring(0, dateStr.indexOf('T'));
    }
    return dateStr;
}

//	（yyyy-MM-dd）
function stringToDateString(str, num) {
    var arr = str.split("-");
    var dd = new Date();
    dd.setMonth(Number(arr[1]) - 1);
    dd.setDate(Number(arr[2]) + num);
    var ddStr = dd.getMonth() + 1 + "/" + dd.getDate();
    return ddStr;
}



